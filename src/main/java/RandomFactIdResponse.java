package main.java;

public class RandomFactIdResponse {
    private int id;

    public RandomFactIdResponse(int id) {
        this.id = id;
    }

    public RandomFactIdResponse() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }


    @Override
    public String toString() {
        return "RandomFactIdResponse{" +
                "id=" + id +
                '}';
    }
}
