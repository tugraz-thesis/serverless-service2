package main.java;

public class FactListSize {

    int size;

    public FactListSize(int size) {
        this.size = size;
    }

    public FactListSize() {
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    @Override
    public String toString() {
        return "FactListSize{" +
                "size=" + size +
                '}';
    }
}
