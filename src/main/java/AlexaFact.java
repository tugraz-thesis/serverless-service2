package main.java;

import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

@Entity
@Table(name="alexa_crypto_fact")
@Cacheable
@org.hibernate.annotations.Cache(usage = CacheConcurrencyStrategy.READ_ONLY)
public class AlexaFact {

    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private int id;

    private String factText;



}
