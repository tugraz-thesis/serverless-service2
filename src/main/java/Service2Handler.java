package main.java;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import java.util.Random;

public class Service2Handler implements RequestHandler<Object, RandomFactIdResponse> {

    // reuse db connection from container outside of handler method
    SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
    Session session = sessionFactory.openSession();

    public RandomFactIdResponse handleRequest(Object input, Context context) {

        long size = (long) session.createQuery("SELECT COUNT(f.id) FROM AlexaFact f").getSingleResult();
        int index = new Random().nextInt(Long.valueOf(size).intValue()) + 1;

        RandomFactIdResponse randomFactIdResponse = new RandomFactIdResponse(index);

        return randomFactIdResponse;

    }


}